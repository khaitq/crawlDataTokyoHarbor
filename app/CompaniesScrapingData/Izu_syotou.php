<?php
namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_flight_infor_keywords;
use App\Model\Wp_tokyo_company_flight_messages;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Wp_tokyo_alert_message;
use Goutte;

class Izu_syotou
{
    /**
     * @desciption get messsage status from website Central_air, handel and insert data in table flight info status.
     * @param $urlIzu_syotou
     */
    public function getMessagesStatusIzu_syotou($urlIzu_syotou){
        $crawler = Goutte::request('GET', $urlIzu_syotou);
        if (!empty($crawler)){
            $statusInfo = $crawler->filter("div#content-main main section.conBox_b div.conBox ul.clr")->each(function ($statusInfo) {
                return $statusInfo;
            });
            if (isset($statusInfo[1]) && !empty($statusInfo[1])){
                $getStatusInfo = $statusInfo[1]->filter('li')->each(function ($li){
                    return $li->text();
                });
                for ($i=0; $i < 2 ; $i++) {
                    unset($getStatusInfo[$i]);
                }
                $getStatusInfo = array_values($getStatusInfo);
                $messages = $getStatusInfo[0].','.$getStatusInfo[1].','.$getStatusInfo[2].','.$getStatusInfo[3].''.$getStatusInfo[4].''.$getStatusInfo[5];

                $getIdCompany = Wp_tokyo_service_companies::where('name', '伊豆諸島開発')->first();
                $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', $getIdCompany['id'])->get()->toArray();

                if (!empty($dataKeyword)){
                    $check = false;
                    foreach ($getStatusInfo as $valueStatus){
                        foreach ($dataKeyword as $valueKeyword){
                            $checkData = strpos($valueStatus, $valueKeyword['keyword']);
                            if ($checkData == true){
                                $check = true;
                            }
                        }
                    }
                    $time = date("Y-m-d H:i:s");
                    if ($check){
                        $data = [
                            'company_id' => $valueKeyword['company_id'],
                            'status_info' => 2,
                            'message' => $messages,
                            'update_time' => $time
                        ];
                    }else{
                        $data = [
                            'company_id' => $valueKeyword['company_id'],
                            'status_info' => 1,
                            'message' => $messages,
                            'update_time' => $time
                        ];
                    }
                    $dataStatus = Wp_tokyo_alert_message::where('company_id', $valueKeyword['company_id'])->first();
                    $update_time = explode(' ', $dataStatus['update_time'])[0];
                    $time = explode(' ',$time)[0];
                    if (!empty($dataStatus['message'])){
                        (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
                    }else{
                        Wp_tokyo_alert_message::create($data);
                    }
                }
            }
        }
    }
}
