<?php

namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_service_companies;
use App\Model\Wp_tokyo_flight_infor_keywords;
use Goutte;

class DataFlightKeyword
{
    /**
     * @description handel and insert data in table flight infor keyword.
     */
    public function insertDataTableFlightInforKeyword(){

        $ANA = ['ANA','遅延', '欠航', '他空港への着陸', '引き返し','悪天候', '台風','降雪', '機材故障','悪天候','条件付きの運航','視界不良','強風','雪','航空管制','台風','噴煙'];
        $central_air = ['新中央航空','火山ガス', '条件付運航', '天候調査中', '天候不良'];
        $tohoair = ['東邦航空','条件付就航', '条件付就航予定', '天候不良', '機材故障', '欠航'];
        $tokaikisen = ['東海汽船','運休','年対低気圧','台風', '海上不良', '欠航', '通過', '天候調査中', '港湾状況不良', '避難勧告', '入港制限', '前線通過', '着岸できない', '繰上出航', '折り返し', '止まり', '機関トラブル', '未定', '途中引き返し', '接岸できない', '折り返す'];
        $nijima = ['新島村', '海上の状態が悪い', '海上の状態が悪く', '早出し運航', '接岸が困難', '欠航', '航行が困難', '台風', '避難', '運休', '戻り就航', '港内の状況が悪い', '未定'];
        $shinshin_kisen = ['神新汽船','天候状況により運航できない場合', '現地港状況は非常に悪く', '欠航', '低気圧通過', '海上不良', '接岸できない', '海上状況', '機関トラブル'];
        $ogasawarakaiun = ['小笠原海運','台風', '海況不良', '運航スケジュール', '運航スケジュール変更', '台風', '延発', '変更', '遅延', '欠航', '出港時間'];
        $izu_syotou = ['伊豆諸島開発', '欠航', '条件付', '台風', '欠航', '臨時運航', '影響', 'スケジュール変更'];
        $getNameAna = $ANA[0];
        $getNameTokaikisen = $tokaikisen[0];
        $getNameOgasawarakaiun = $ogasawarakaiun[0];
        $getNameCentral_air = $central_air[0];
        $getNameShinshin_kisen = $shinshin_kisen[0];
        $getNameTohoair = $tohoair[0];
        $getNameNijima = $nijima[0];
        $getNameIzu_syutou = $izu_syotou[0];
        array_shift($ANA);
        array_shift($tokaikisen);
        array_shift($ogasawarakaiun);
        array_shift($central_air);
        array_shift($shinshin_kisen);
        array_shift($tohoair);
        array_shift($nijima);
        array_shift($izu_syotou);

        /*
         * @description get id company ANA and insert data table Wp_tokyo_flight_infor_keywords
         */
        if(!empty($ANA)){
            foreach ($ANA as $value) {
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameAna)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword ANA'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Tokaikisen and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($tokaikisen)){
            foreach ($tokaikisen as $value) {
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameTokaikisen)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Tokaikisen'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Ogasawarakaiun and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($ogasawarakaiun)){
            foreach ($ogasawarakaiun as $value) {
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameOgasawarakaiun)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Ogasawarakaiun'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Central_air and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($getNameCentral_air)){
            foreach ($central_air as $value) {
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameCentral_air)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Central_air'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company shinshin_kisen and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($shinshin_kisen)){
            foreach ($shinshin_kisen as $value){
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameShinshin_kisen)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Shinshin_kisen'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Tohoair and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($tohoair)){
            foreach ($tohoair as $value){
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameTohoair)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Tohoair'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Tohoair and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($nijima)){
            foreach ($nijima as $value){
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameNijima)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Nijima'.(($result) ? 'success' : 'fails');
        }

        /*
         * @description get id company Izu_syotou and insert data table Wp_tokyo_flight_infor_keywords
         */
        if (!empty($izu_syotou)){
            foreach ($izu_syotou as $value){
                $getIdServiceCompany = Wp_tokyo_service_companies::where('name', $getNameIzu_syutou)->value('id');
                $data = [
                    'company_id' => $getIdServiceCompany,
                    'keyword' => $value,
                ];
                $result = Wp_tokyo_flight_infor_keywords::create($data);
            }
            echo 'get data keyword Izu_syotou'.(($result) ? 'success' : 'fails');
        }
    }
}
