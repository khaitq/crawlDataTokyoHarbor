<?php
namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_alert_message;
use App\Model\Wp_tokyo_company_flight_messages;
use App\Model\Wp_tokyo_route_informations;
use App\Model\Wp_tokyo_stations;
use App\Model\Wp_tokyo_flight_infor_keywords;
use Goutte;

class Tokaikisen
{
    /*
     * @description function crawl status from website Tokikaisen
     */
    public function getMessagesStatusTokaikisen($urlTokaikisen){

        $crawler = Goutte::request('GET', $urlTokaikisen);
        $status = $crawler->filter('div.attention div.wrap div.attention__box')->each(function ($status){
            return $status->text();
        })[0];

        /**
         * @description Get text tr infor in table by id div
         */
        $tableInfors = $crawler->filter("div.scheduleIsland__section.sp--js--accordion")->each(function ($tableInfor){
            return $tableInfor;
        });

        $dataInsert = [];
        for($i = 0; $i < count($tableInfors); $i++) {
            $table = $tableInfors[$i];

            $title = $table->filter('h3')->each(function($title){
                return $title->text();
            });

            $dataTable = $table->filter('div.scheduleTable div.scheduleTable__main table.stable')->each(function ($dataTable){
                return $dataTable;
            });

            for ($j = 0; $j < count($dataTable); $j++){
                $dataInfors = $dataTable[$j];
                $tr = $dataInfors->filter('tr')->each(function ($tr){
                    return $tr;
                });

                if (!empty($tr)){
                    $th = $tr[0]->filter('th')->each(function($th) {
                        return $th->text();
                    });
                }
                $titleRoute = explode('行き',$title[0])[0];
                $titleSpecial = explode('〜',$titleRoute)[0];
                if($th[1] == "発地") {
                    for($t = 1; $t < count($tr); $t++) {
                        $td = $tr[$t]->filter('td')->each(function($td) {
                            return $td->text();
                        });
                        if (!empty($td)){
                            !empty($td[3] == '---') ? $port = $titleSpecial : $port = explode("港",$td[3])[0];
                            $port = explode('行', $port)[0];
                            $port = explode('（黒根）', $port)[0];
                            $data = [
                                'departure_time' => $td[0],
                                'departure' => explode("発", $td[1])[0],
                                'destination' => $port,
                                'title' => $titleSpecial,
                                'status' => $td[4],
                                'remarks' => $td[5]
                            ];
                            $dataInsert[] = $data;
                        }
                    }
                }

                if($th[1] == "行き先") {
                    for($t = 1; $t < count($tr); $t++) {
                        $td = $tr[$t]->filter('td')->each(function($td) {
                            return $td->text();
                        });
                        if (!empty($td)){
                            !empty($td[3] == '---') ? $port = $titleSpecial : $port =  explode("港",$td[3])[0];
                            $port = explode('（黒根）', $port)[0];
                            $data = [
                                'departure_time' => $td[0],
                                'departure' => $port,
                                'destination' => explode("行", $td[1])[0],
                                'title' => $titleSpecial,
                                'status' => $td[4],
                                'remarks' => $td[5]
                            ];
                            $dataInsert[] = $data;
                        }
                    }
                }
            }
        }
        if (isset($dataInsert) && !empty($dataInsert)) {
            foreach ($dataInsert as $key => $value) {

                $departure = Wp_tokyo_stations::where('name', 'like','%'.$value['departure'].'%')
                    ->where('type','ship')->first();

                $arrival_id = Wp_tokyo_stations::where('name', 'like','%'.$value['destination'].'%')
                    ->where('parent_name', 'like', '%'.$value['title'].'%')
                    ->where('type','ship')->first();

                $destinationTitle = Wp_tokyo_stations::where('name', 'like','%'.$value['title'].'%')
                    ->where('parent_name', 'like', '%'.$value['title'].'%')
                    ->where('type','ship')->first();

                if (empty($arrival_id)) {
                    $arrival_id = Wp_tokyo_stations::where('parent_name', 'like','%'.$value['destination'].'%')
                        ->where('parent_name', $value['title'])
                        ->where('type','ship')->first();
                }
                $date = date('Y-m-d');
                $departure_time = $date.' '.$value['departure_time']. ':00';
                Wp_tokyo_route_informations::where('service_company_id' , 1)
                    ->where('departure_time', $departure_time)
                    ->where('departure_id', $departure['id'])
                    ->where('arrival_id', $destinationTitle['id'])
                    ->update([
                        'departure_id' => $departure['id'],
                        'arrival_id'=> $arrival_id['id'],
                        'remark' => $value['remarks']
                    ]);

                $dataStatus = [
                    'status' => '0'.$value['status'],
                    'remark' => '0'.$value['remarks'],
                    'departure_id' => $departure['id'],
                    'arrival_id' => $arrival_id['id'],
                    'departure_time' => $departure_time
                ];

                $totalDataStatus[] = $dataStatus;
            }
            $this->checkKeyWord($totalDataStatus, $status);
        }
    }

    /*
     * @description check keyword tokaikisen.
     */
    public function checkKeyWord($totalDataStatus, $status){
        $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', 1)->get()->toArray();
        foreach ($dataKeyword as $keyword){
            $check = false;
            $checkStatus = strpos($status,$keyword['keyword']);
            if ($checkStatus == true){
                $check = true;
                break;
            }
            foreach ($totalDataStatus as $value){
                if ($value['status'] != '0'){
                    $checkdata = strpos($value['status'],$keyword['keyword']);
                    if ($value['status'] == '0---'){
                        $checkdata = strpos($value['remark'],$keyword['keyword']);
                        $checkdata !== false ? $value['status'] = $value['remark'] : null;
                    }
                    if ($checkdata !== false){
                        $routeInfo = Wp_tokyo_route_informations::where('service_company_id', 1)
                            -> where('departure_time',$value['departure_time'])
                            -> where('departure_id', $value['departure_id'])
                            -> where('arrival_id', $value['arrival_id'])
                            -> first();
                        if (!empty($routeInfo)){
                            $routeInfo->update(['status' => 2]);
                        }

                        $dataStatusInsert = [
                            'company_id' => 1,
                            'status_info' => 2,
                            'route_information_id' => $routeInfo['id'],
                            'message' => explode('0',$value['status'])[1],
                        ];

                        $idFlightStatus = !empty($routeInfo['id']) ?  $routeInfo['id'] : -1 ;
                        $dataFlightStatus = Wp_tokyo_company_flight_messages::where('route_information_id', $idFlightStatus)->first();
                        if (!empty($dataFlightStatus['message'])){
                            $dataFlightStatus->update($dataStatusInsert);
                        }else{
                            Wp_tokyo_company_flight_messages::create($dataStatusInsert);
                        }
                    }
                }
            }
        }
        $time = date("Y-m-d H:i:s");
        if ($check){
            $data = [
                'company_id' => 1,
                'status_info' => 2,
                'message' => $status,
                'update_time' => $time,
            ];
        }else{
            $data = [
                'company_id' => 1,
                'status_info' => 1,
                'message' => $status,
                'update_time' => $time,
            ];
        }
        $dataStatus = Wp_tokyo_alert_message::where('company_id', 1)->first();
        $update_time = explode(' ', $dataStatus['update_time'])[0];
        $time = explode(' ',$time)[0];
        if (!empty($dataStatus['message'])){
            (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
        }else{
            Wp_tokyo_alert_message::create($data);
        }
    }
}

