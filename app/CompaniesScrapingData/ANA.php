<?php
namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_alert_message;
use App\Model\Wp_tokyo_flight_infor_keywords;
use App\Model\Wp_tokyo_company_flight_messages;
use App\Model\Wp_tokyo_service_companies;
use Spatie\Browsershot\Browsershot;
use Goutte;
use Symfony\Component\DomCrawler\Crawler;

class ANA
{
    /**
     * @description get message status from website ANA, handel and insert data in table flight info status.
     * @param $urlApi
     */
    public function getMessagesStatusANA(){
        $date = Date('Y-m-d');
        $datetime = str_replace("-","", $date);
        $urlApi = 'https://www.ana.co.jp/fs/dom/jp/result.html?mode=1&requestDate='.$datetime.'&depAirportSelect=HND&arrAirportSelect=HAC&txtDepAirport=東京(羽田)&txtArrAirport=八丈島';
        $crawler = Goutte::request('GET', $urlApi);
        $browserShot = Browsershot::url($urlApi)->bodyHtml();
        $crawlerJavaScrip = new Crawler($browserShot);
        $roureInfoStatus = $crawlerJavaScrip->filter('table#resultC')->each(function($table) {
            return $table->text();
        })[0];
        $a = explode(' ', $roureInfoStatus);

        /*-----------------------------------------------------------------------*/
        $statusInfo = $crawler->filter("div#statusLead div#stLeadL p")->each(function ($statusInfo) {
            return $statusInfo->text();
        })[0];

        $getIdCompany = Wp_tokyo_service_companies::where('name','like','%'.'ANA'.'%')->first();
        $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', $getIdCompany['id'])->get()->toArray();
        $check = false;
        foreach ($dataKeyword as $value){
            $checkData = strpos($statusInfo, $value['keyword']);
            if ($checkData == true){
                $check = true;
                break;
            }
        }
        $time = date("Y-m-d H:i:s");
        if ($check){
            $data = [
                'company_id' => $value['company_id'],
                'status_info' => 2,
                'message' => $statusInfo,
                'update_time' => $time
            ];
        }else{
            $data = [
                'company_id' => $value['company_id'],
                'status_info' => 1,
                'message' => $statusInfo,
                'update_time' => $time
            ];
        }
        $dataStatus = Wp_tokyo_alert_message::where('company_id', $value['company_id'])->first();
        $update_time = explode(' ', $dataStatus['update_time'])[0];
        $time = explode(' ',$time)[0];
        if (!empty($dataStatus['message'])){
            (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
        }else{
            Wp_tokyo_alert_message::create($data);
        }
    }
}
