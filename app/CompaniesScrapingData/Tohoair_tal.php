<?php
namespace App\CompaniesScrapingData;

use Goutte;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Wp_tokyo_flight_infor_keywords;
use App\Model\Wp_tokyo_company_flight_messages;
use App\MOdel\Wp_tokyo_alert_message;

class Tohoair_tal
{
    /**
     * @description get messages status from website Izu_syotou, handel and insert data in table flight info status.
     * @param $urlTohoair
     */
    public function getMessagesStatusTohoair($urlTohoair){
        $crawler = Goutte::request('GET', $urlTohoair);
        $statusInfo = $crawler->filter("div#topics_category div.box p")->each(function ($statusInfo) {
            return $statusInfo->text();
        })[2];

            $getIdCompany = Wp_tokyo_service_companies::where('name', 'like','%'.'東邦航空'.'%')->first();
        $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', $getIdCompany['id'])->get()->toArray();
        $check = false;
        if (!empty($statusInfo)){
            foreach ($dataKeyword as $value){
                $checkData = strpos($statusInfo, $value['keyword']);
                if ($checkData == true){
                    $check = true;
                    break;
                }
            }
            $time = date("Y-m-d H:i:s");
            if ($check){
                $data = [
                    'company_id' => $value['company_id'],
                    'status_info' => 2,
                    'message' => $statusInfo,
                    'update_time' => $time
                ];
            }else{
                $data = [
                    'company_id' => $value['company_id'],
                    'status_info' => 1,
                    'message' => $statusInfo,
                    'update_time' => $time
                ];
            }
            $dataStatus = Wp_tokyo_alert_message::where('company_id',$value['company_id'])->first();
            $update_time = explode(' ', $dataStatus['update_time'])[0];
            $time = explode(' ',$time)[0];
            if (!empty($dataStatus['message'])){
                (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
            }else{
                Wp_tokyo_alert_message::create($data);
            }
        }
    }
}
