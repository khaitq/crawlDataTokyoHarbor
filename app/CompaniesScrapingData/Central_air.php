<?php
namespace App\CompaniesScrapingData;

use Goutte;
use App\Model\Wp_tokyo_flight_infor_keywords;
use App\Model\Wp_tokyo_company_flight_messages;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Wp_tokyo_alert_message;

class Central_air
{
    /**
     * @description get message status from website Central-air, handel and insert data in table flight info status.
     * @param $urlCentral_air
     */
    public function getMessagesStatusCentral_air($urlCentral_air){
        $crawler = Goutte::request('GET', $urlCentral_air);
        $statusInfo = $crawler->filter("table")->each(function ($statusInfo) {
            return $statusInfo->text();
        })[0];

        $getIdCompany = Wp_tokyo_service_companies::where('name', '新中央航空')->first();
        $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', $getIdCompany['id'])->get()->toArray();
        if (!empty($statusInfo)){
            $check = false;
            foreach ($dataKeyword as $valueKeyword){
                $checkData = strpos($statusInfo, $valueKeyword['keyword']);
                if ($checkData == true){
                    $check = true;
                    break;
                }
            }
            $time = date("Y-m-d H:i:s");
            if ($check){
                $data = [
                    'company_id' => $valueKeyword['company_id'],
                    'status_info' => 2,
                    'message' => $statusInfo,
                    'update_time' => $time
                ];
            }else{
                $data = [
                    'company_id' => $valueKeyword['company_id'],
                    'status_info' => 1,
                    'message' => $statusInfo,
                    'update_time' => $time
                ];
            }
            $dataStatus = Wp_tokyo_alert_message::where('company_id', $valueKeyword['company_id'])->first();
            $update_time = explode(' ', $dataStatus['update_time'])[0];
            $time = explode(' ',$time)[0];
            if (!empty($dataStatus['message'])){
                (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
            }else{
                Wp_tokyo_alert_message::create($data);
            }
        }
    }
}
