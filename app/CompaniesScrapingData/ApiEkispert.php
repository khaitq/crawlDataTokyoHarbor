<?php
namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_route_informations;
use App\Model\Wp_tokyo_stations;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Route_api_mapping;
use App\Model\Wp_tokyo_route_transfer_information;
use Goutte;

class ApiEkispert
{
    /**
     * @function CrawlApiGetData.
     * @description Get data with api from http://api.ekispert.jp
     * @param $baseURL
     */
    public function
    crawlDataApi($baseURL)
    {
        //Search list, From-to table route_api_mapping
        $serviceCompanies = Wp_tokyo_service_companies::all()->toArray();
        $port_list = Route_api_mapping::all()->toArray();
        $stations = Wp_tokyo_stations::all()->toArray();
        $date = date('Y-m-d');
        $date_start = strtotime($date);
        $date_end = strtotime('2020-03-30');
        for ($run_date = $date_start; $run_date <= $date_end; $run_date = $run_date + 86400) {
            $thedate = date('Ymd', $run_date);
            //Data extraction
            foreach ($port_list as $onePort) {
                $one = $onePort['departure_code'];//from station
                $onedes = $onePort['arrival_code'];//to station
                $starttimeBack = true;
                $nextflag = true;
                $starttime = 0;

                //A => B route search
                $getUrl = $baseURL . "&viaList={$one}:{$onedes}&time=0000&date=" . $thedate;
                $result = file_get_contents($getUrl);
                $result = json_decode($result, true);
                if (isset($result['ResultSet']['Course']['searchType'])) {
                    //If there is only one result, unify the configuration
                    $result['ResultSet']['Course'] = array($result['ResultSet']['Course']);
                }

                if (!empty($result['ResultSet']['Course'])){

                    foreach ($result['ResultSet']['Course'] as $key => $oneVal) {
                        $points = $oneVal['Route']['Point'];
                        $lines = $oneVal['Route']['Line'];
                        if (!empty($oneVal['Route']['Line']['Name'])){
                            $corporation =  $oneVal['Route']['Line']['Name'];
                        }
                        $departureTime = $oneVal['Route']['transferCount'] > 0 ?
                            $lines[0]['DepartureState']['Datetime']['text'] :
                            $lines['DepartureState']['Datetime']['text'];

                        $departureTime = str_replace("T"," ", $departureTime);
                        $departureTime = str_replace("+09:00","", $departureTime);
                        $starttime = (isset($starttime) && $starttime > $departureTime) ? $starttime : $departureTime;
                        $arrivalTime = $oneVal['Route']['transferCount'] > 0 ?
                            $lines[count($lines)-1]['ArrivalState']['Datetime']['text'] :
                            $lines['ArrivalState']['Datetime']['text'];
                        $arrivalTime = str_replace("T"," ", $arrivalTime);
                        $arrivalTime = str_replace("+09:00","", $arrivalTime);

                        if ($oneVal['Route']['transferCount'] > 0){
                            $count_num = count($oneVal['Price']) - 1;

                            $checkTypeTransportation = true;
                            $typeTransportation = ['ship', 'plane'];
                            foreach ($lines as $line) {
                                if(!is_numeric(array_search($line["Type"], $typeTransportation))) {
                                    $checkTypeTransportation = false;
                                }
                            }
                            if($checkTypeTransportation) {
                                if (!empty($stations)){
                                    foreach ($stations as $station) {
                                        if($station['code'] == $points[0]['Station']['code']) {
                                            $idDeparture = $station['id'];
                                        }

                                        if($station['code'] == $points[count($points)-1]['Station']['code']) {
                                            $idArrival = $station['id'];
                                        }
                                    }
                                }
                                $price = $oneVal['Price'][$count_num]['Oneway'];

                                $dataInsert = [];
                                $dataInsert['departure_id'] = $idDeparture;
                                $dataInsert['arrival_id'] =  $idArrival;
                                $dataInsert['date'] = $date;
                                $dataInsert['departure_time'] = $departureTime;
                                $dataInsert['arrival_time'] = $arrivalTime;
                                $dataInsert['price'] = $price;
                                $dataInsert['transportation_type_id'] = 0;
                                $dataInsert['corporation'] = $corporation;
                                $dataInsert['ship_numbers'] = null;
                                $dataInsert['service_company_id'] = 0;
                                $dataInsert['price_label'] = $price;
                                $dataInsert['status'] = 1;
                                $dataInsert['created_at'] = $date;
                                $dataInsert['updated_at'] = $date;
                                $dataInsert['is_transfer'] = 1;
                                $directRoute = Wp_tokyo_route_informations::create($dataInsert);
                                $stage = 1;
                                $index = 0;
                                foreach ($lines as $line) {
                                    $departureTime = $line['DepartureState']['Datetime']['text'];
                                    $departureTime = str_replace("T"," ", $departureTime);
                                    $departureTime = str_replace("+09:00","", $departureTime);
                                    $arrivalTime = $line['ArrivalState']['Datetime']['text'];
                                    $arrivalTime = str_replace("T"," ", $arrivalTime);
                                    $arrivalTime = str_replace("+09:00","", $arrivalTime);
                                    $count = 0;
                                    $routePoints= [];
                                    for($i = $index; $i < count($points); $i++) {
                                        if($count < 2) {
                                            $routePoints[] = $points[$i];
                                        }
                                        $count++;
                                    }
                                    if(count($routePoints) == 2) {
                                        $departureKey = array_search($routePoints[0]['Station']['code'], array_column($stations, 'code'));
                                        $arrivalKey = array_search($routePoints[1]['Station']['code'], array_column($stations, 'code'));
                                        $departure = $stations[$departureKey];
                                        $arrival = $stations[$arrivalKey];
                                        $route = Wp_tokyo_route_informations::where('departure_id', $departure['id'])
                                            ->where('arrival_id', $arrival['id'])
                                            ->where('departure_time', $departureTime)
                                            ->where('arrival_time', $arrivalTime)
                                            ->first();
                                        $dataInsert = [];
                                        $dataInsert['route_id'] = $directRoute['id'];
                                        $dataInsert['transfer_id'] = $route['id'];
                                        $dataInsert['stage'] = $stage;
                                        Wp_tokyo_route_transfer_information::create($dataInsert);
                                        $stage++;
                                        $index++;
                                    }

                                }
                            }

                        }
                    }

                }
                var_dump(isset($result['ResultSet']['Course']) && count($result['ResultSet']['Course'])>=2, $starttime);
                if(isset($result['ResultSet']['Course']) && count($result['ResultSet']['Course'])>=2 && $starttime > 0) {
                    // loop if return result > 2 rows
                    $count_run = 1;
                    do {
                        $getURL = $baseURL . "&viaList={$one}:{$onedes}&date=" . $thedate;
                        var_dump($nextflag);
                        $this->loopSearch($getURL,$starttime,$nextflag,$one,$onedes,$thedate);
                        $count_run++;
                        if($count_run >= 20){
                            $nextflag = false;
                        }
                    } while($nextflag);
                }
            }

            dd('ok');
        }


    }
    /**
     * @function loopSearch
     * @description loop search get data in api from http://api.ekispert.jp
     * @param $getURL
     * @param $starttime
     * @param $starttimeBack
     * @param $nextflag
     * @param $one
     * @param $onedes
     * @param $thedate
     */
    public function loopSearch($getURL,&$starttime,&$nextflag,$one,$onedes,$thedate)
    {
        $nextflag = false;
        var_dump("Loop run");
        $starttime_tmp = str_replace("T"," ", $starttime);
        $starttime_tmp = str_replace("+09:00","", $starttime_tmp);
        $starttime_tmp = Date('Hi',strtotime($starttime_tmp) + 60);
        $getUrl = $getURL . "&time={$starttime_tmp}";
        $result = file_get_contents($getUrl);
        $result = json_decode($result,true);

        if(isset($result['ResultSet']['Course']['searchType'])){
            //If there is only one result, unify the configuration
            $result['ResultSet']['Course'] = array($result['ResultSet']['Course']);
        }

        if (!empty($result['ResultSet']['Course'])){
            foreach($result['ResultSet']['Course'] as $key => $oneVal) {
                //direct route only will put to database
                if($oneVal['Route']['transferCount'] > 0) {
                    $nextflag = true;
                    $count_num = count($oneVal['Price']) - 1;
                    $date = Date('Y-m-d');
                    $points = $oneVal['Route']['Point'];
                    $lines = $oneVal['Route']['Line'];
                    if (!empty($oneVal['Route']['Line']['Name'])){
                        $corporation = $oneVal['Route']['Line']['Name'];
                    }
                    $checkTypeTransportation = true;
                    $typeTransportation = ['ship', 'plane'];
                    foreach ($lines as $line) {
                        if(!is_numeric(array_search($line["Type"], $typeTransportation))) {
                            $checkTypeTransportation = false;
                        }
                    }
                    if($checkTypeTransportation) {
                        if (!empty($stations)){
                            foreach ($stations as $station) {
                                if($station['code'] == $points[0]['Station']['code']) {
                                    $idDeparture = $station['id'];
                                }

                                if($station['code'] == $points[count($points)-1]['Station']['code']) {
                                    $idArrival = $station['id'];
                                }
                            }
                        }

                        $departureTime = $oneVal['Route']['Line']['DepartureState']['Datetime']['text'];
                        $departureTimeTmp = Date('Ymd',strtotime(str_replace("+09:00","", $departureTime)));
                        // Check new date.
                        if ($departureTimeTmp > $thedate) {
                            $nextflag = false;
                            continue;
                        }
                        $starttime = (isset($starttime) && $starttime > $departureTime) ? $starttime : $departureTime;

                        $departureTime = str_replace("T"," ", $departureTime);
                        $departureTime = str_replace("+09:00","", $departureTime);
                        $arrivalTime = $lines[count($lines)-1]['ArrivalState']['Datetime']['text'];
                        $arrivalTime = str_replace("T"," ", $arrivalTime);
                        $arrivalTime = str_replace("+09:00","", $arrivalTime);
                        $price = $oneVal['Price'][$count_num]['Oneway'];

                        $dataInsert = [];
                        $dataInsert['departure_id'] = $idDeparture;
                        $dataInsert['arrival_id'] =  $idArrival;
                        $dataInsert['date'] = $date;
                        $dataInsert['departure_time'] = $departureTime;
                        $dataInsert['arrival_time'] = $arrivalTime;
                        $dataInsert['price'] = $price;
                        $dataInsert['transportation_type_id'] = 0;
                        $dataInsert['corporation'] = $corporation;
                        $dataInsert['ship_numbers'] = null;
                        $dataInsert['service_company_id'] = 0;
                        $dataInsert['price_label'] = $price;
                        $dataInsert['status'] = 1;
                        $dataInsert['created_at'] = $date;
                        $dataInsert['updated_at'] = $date;
                        $dataInsert['is_transfer'] = 1;
                        $directRoute = Wp_tokyo_route_informations::create($dataInsert);
                        $stage = 1;
                        $index = 0;
                        foreach ($lines as $line) {
                            $departureTime = $line['DepartureState']['Datetime']['text'];
                            $departureTime = str_replace("T"," ", $departureTime);
                            $departureTime = str_replace("+09:00","", $departureTime);
                            $arrivalTime = $line['ArrivalState']['Datetime']['text'];
                            $arrivalTime = str_replace("T"," ", $arrivalTime);
                            $arrivalTime = str_replace("+09:00","", $arrivalTime);
                            $count = 0;
                            $routePoints= [];
                            for($i = $index; $i < count($points); $i++) {
                                if($count < 2) {
                                    $routePoints[] = $points[$i];
                                }
                                $count++;
                            }
                            if(count($routePoints) == 2) {
                                $departureKey = array_search($routePoints[0]['Station']['code'], array_column($stations, 'code'));
                                $arrivalKey = array_search($routePoints[1]['Station']['code'], array_column($stations, 'code'));
                                $departure = $stations[$departureKey];
                                $arrival = $stations[$arrivalKey];
                                $route = Wp_tokyo_route_informations::where('departure_id', $departure['id'])
                                    ->where('arrival_id', $arrival['id'])
                                    ->where('departure_time', $departureTime)
                                    ->where('arrival_time', $arrivalTime)
                                    ->first();
                                $dataInsert = [];
                                $dataInsert['route_id'] = $directRoute['id'];
                                $dataInsert['transfer_id'] = $route['id'];
                                $dataInsert['stage'] = $stage;
                                Wp_tokyo_route_transfer_information::create($dataInsert);
                                $stage++;
                                $index++;
                            }
                        }
                    }
                }
            }
        }
    }
}
