<?php
namespace App\CompaniesScrapingData;

use App\Console\Commands\TwitterAPIExchange;
use App\Model\Wp_tokyo_flight_infor_keywords;
use App\Model\Wp_tokyo_company_flight_messages;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Wp_tokyo_alert_message;
use Goutte;

class Ogasawarakaiun
{
    /**
     * @description get message status from website Ogasawarakaiun, handel and insert data in table flight info status.
     * @param $urlOgasawarakaiun
     */
    public function getMessagesStatusOgasawarakaiun(){
        //get status from html website ogasawarakaiun
        $urlOgasawarakaiun = 'https://www.ogasawarakaiun.co.jp/info/?page_id=9';
        $crawler = Goutte::request('GET', $urlOgasawarakaiun);
        $statusInfo = $crawler->filter("li")->each(function ($statusInfo) {
            return $statusInfo->text();
        })[0];

        //call api Ogasawarakaiun on Twitter
        $settings = [
            'oauth_access_token' => "1212666220807245826-GBsPOx73Q4VY9FHmvnaUGhqFJe7r1i",
            'oauth_access_token_secret' => "J0jSuDiiMdm4tiPsNMgNWlLk7Qev75sD0TWZbQ3AfAfpW",
            'consumer_key' => "Lyk820G5qfZCoiEErONnwN5Hv",
            'consumer_secret' => "hghvDu9Nh8dBTJelF5H1nNZlRHPIs17ZrDSe5hUg8NzS9loOIB"
        ];
        $url = 'https://api.twitter.com/1.1/users/show.json';
        $getfield = '?screen_name=ogasawrakaiun';
        $requestMethod = 'GET';
        $twitter = new TwitterAPIExchange($settings);
        $dataApiStatus = $twitter->setGetField($getfield)->buildOauth($url, $requestMethod)->performRequest();
        $dataApiStatus = json_decode($dataApiStatus, true);

        //handel, checking keyword have in string data and insert data in table flight_info_status
        if (!empty($statusInfo) && !empty($dataApiStatus['status']['text'])){
            $getIdCompany = Wp_tokyo_service_companies::where('name', '小笠原海運')->first();
            $dataKeyword = Wp_tokyo_flight_infor_keywords::where('company_id', $getIdCompany['id'])->get()->toArray();
            $dataStatusInfo = $statusInfo.'---- '. $dataApiStatus['status']['text'];
            $check = false;
            foreach ($dataKeyword as $value){
                $checkData = strpos($dataStatusInfo, $value['keyword']);
                if ($checkData == true){
                    $check = true;
                    break;
                }
            }
            $time = date("Y-m-d H:i:s");
            if ($check){
                $data = [
                    'company_id' => $value['company_id'],
                    'status_info' => 2,
                    'message' => $dataStatusInfo,
                    'update_time' => $time
                ];
            }else{
                $data = [
                    'company_id' => $value['company_id'],
                    'status_info' => 1,
                    'message' => $dataStatusInfo,
                    'update_time' => $time
                ];
            }
            $dataStatus = Wp_tokyo_alert_message::where('company_id', $value['company_id'])->first();
            $update_time = explode(' ', $dataStatus['update_time'])[0];
            $time = explode(' ',$time)[0];
            if (!empty($dataStatus['message'])){
                (!empty ($update_time) == $time) ? $dataStatus->update($data) : Wp_tokyo_alert_message::create($data);
            }else{
                Wp_tokyo_alert_message::create($data);
            }
        }
    }
}
