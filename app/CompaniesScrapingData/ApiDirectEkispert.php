<?php
namespace App\CompaniesScrapingData;

use App\Model\Wp_tokyo_route_informations;
use App\Model\Wp_tokyo_stations;
use App\Model\Wp_tokyo_service_companies;
use App\Model\Route_api_mapping;
use Goutte;

class ApiDirectEkispert
{
    /**
     * @function CrawlApiGetData.
     * @description Get data with api from http://api.ekispert.jp
     * @param $baseURL
     */
    public function crawlDataApi($baseURL)
    {
        //Search list, From-to table route_api_mapping
        $serviceCompanies = Wp_tokyo_service_companies::all()->toArray();
        $port_list = Route_api_mapping::all()->toArray();
        $stations = Wp_tokyo_stations::all()->toArray();
        $date = date('Y-m-d');
        $date_start = strtotime($date);
        $date_end = strtotime('2020-03-30');
        $datas = [];
        for ($run_date = $date_start; $run_date <= $date_end; $run_date = $run_date + 86400) {
            $thedate = date('Ymd', $run_date);
            //Data extraction
            foreach ($port_list as $onePort) {
                $one = $onePort['departure_code'];//from station
                $onedes = $onePort['arrival_code'];//to station
                $starttimeBack = true;
                $nextflag = true;
                $starttime = 0;

                //A => B route search
                $getUrl = $baseURL . "&viaList={$one}:{$onedes}&time=0000&date=" . $thedate;
                $result = file_get_contents($getUrl);
                $result = json_decode($result, true);
                if (isset($result['ResultSet']['Course']['searchType'])) {
                    //If there is only one result, unify the configuration
                    $result['ResultSet']['Course'] = array($result['ResultSet']['Course']);
                }

                if (!empty($result['ResultSet']['Course'])){

                    foreach ($result['ResultSet']['Course'] as $key => $oneVal) {
//                        direct route only will put to database
                        if ($oneVal['Route']['transferCount'] == 0) {
                            $count_num = count($oneVal['Price']) - 1;

                            $corporation = $oneVal['Route']['Line']['Name'];
                            $price = $oneVal['Price'][$count_num]['Oneway'];
                            $typeName = $oneVal['Route']['Line']['TypicalName'];
                            $datetimebegin = $oneVal['Route']['Line']['DepartureState']['Datetime']['text'];
                            $datetimebegin = str_replace("T"," ", $datetimebegin);
                            $datetimebegin = str_replace("+09:00","", $datetimebegin);
                            $datetimeend = $oneVal['Route']['Line']['ArrivalState']['Datetime']['text'];
                            $datetimeend = str_replace("T"," ", $datetimeend);
                            $datetimeend = str_replace("+09:00","", $datetimeend);
                            $timeOnBoard = $oneVal['Route']['Line']['timeOnBoard'];
                            if (!empty($oneVal['Route']['Line']['Number'])){
                                $ship_numbers = $oneVal['Route']['Line']['Number'];
                            }
                            $starttime = (isset($starttime) && $starttime > $datetimebegin) ? $starttime : $datetimebegin;
                            $csv = "{$one},{$onedes},{$date},{$datetimebegin},{$datetimeend},{$timeOnBoard},{$price},{$corporation},{$ship_numbers},{$typeName}";
                            $data = explode(',', $csv);
                            $nameCompany = explode("・", $data[7])[0];
                            $typeService = explode("(",$data[9])[0];

                            $idType = 3;
                            if (!empty($typeService == '超高速船')){
                                $idType = 2;
                            }elseif (!empty($typeService == '旅客船')){
                                $idType = 1;
                            }
                            if (!empty($stations)){
                                foreach ($stations as $station) {
                                    if($station['code'] == $data[0]) {
                                        $idDeparture = $station['id'];
                                    }

                                    if($station['code'] == $data[1]) {
                                        $idArrival = $station['id'];
                                    }
                                }
                            }

                            if (!empty($serviceCompanies)){
                                foreach ($serviceCompanies as $serviceCompany){
                                    if ($serviceCompany['name'] == $nameCompany){
                                        $idServiceCompany = $serviceCompany['id'];
                                    }
                                }
                            }

                            $dataInsert = [];
                            $dataInsert['departure_id'] = $idDeparture;
                            $dataInsert['arrival_id'] =  $idArrival;
                            $dataInsert['date'] = $data[2];
                            $dataInsert['departure_time'] = $data[3];
                            $dataInsert['arrival_time'] = $data[4];
                            $dataInsert['price'] = $price;
                            $dataInsert['transportation_type_id'] = $idType;
                            $dataInsert['corporation'] = $nameCompany;
                            $dataInsert['ship_number'] = $data[8];
                            $dataInsert['service_company_id'] = $idServiceCompany;
                            $dataInsert['price_label'] = $data[6];
                            $dataInsert['status'] = 1;
                            $dataInsert['created_at'] = $date;
                            $dataInsert['updated_at'] = $date;
                            $datas[] = $dataInsert;
                        }
                    }

                }

                if(isset($result['ResultSet']['Course']) && count($result['ResultSet']['Course'])>=2 && $starttime > 0) {
                    // loop if return result > 2 rows
                    $count_run = 1;
                    do {
                        $getURL = $baseURL . "&viaList={$one}:{$onedes}&date=" . $thedate;
                        $this->loopSearch($getURL,$starttime,$nextflag,$one,$onedes,$thedate);
                        $count_run++;
                        if($count_run >= 20){
                            $nextflag = false;
                        }
                    } while($nextflag);
                }
            }
        }

        if(count($datas) != 0){
            Wp_tokyo_route_informations::insert($datas);
            return ('Insert ' . count($datas) . ' rows successful');
        }

    }
    /**
     * @function loopSearch
     * @description loop search get data in api from http://api.ekispert.jp
     * @param $getURL
     * @param $starttime
     * @param $starttimeBack
     * @param $nextflag
     * @param $one
     * @param $onedes
     * @param $thedate
     */
    public function loopSearch($getURL,&$starttime,&$nextflag,$one,$onedes,$thedate)
    {
        $nextflag = false;
        $starttime_tmp = str_replace("T"," ", $starttime);
        $starttime_tmp = str_replace("+09:00","", $starttime_tmp);
        $starttime_tmp = Date('Hi',strtotime($starttime_tmp) + 60);
        $getUrl = $getURL . "&time={$starttime_tmp}";
        $result = file_get_contents($getUrl);
        $result = json_decode($result,true);

        if(isset($result['ResultSet']['Course']['searchType'])){
            //If there is only one result, unify the configuration
            $result['ResultSet']['Course'] = array($result['ResultSet']['Course']);
        }

        if (!empty($result['ResultSet']['Course'])){
            foreach($result['ResultSet']['Course'] as $key => $oneVal) {
                //direct route only will put to database
                if($oneVal['Route']['transferCount'] == 0) {
                    $nextflag = true;
                    $count_num = count($oneVal['Price']) - 1;
                    $corporation = $oneVal['Route']['Line']['Name'];
                    $price = $oneVal['Price'][$count_num]['Oneway'];
                    $typeName = $oneVal['Route']['Line']['TypicalName'];
                    $datetimebegin = $oneVal['Route']['Line']['DepartureState']['Datetime']['text'];
                    $datetimebegin_tmp = Date('Ymd',strtotime(str_replace("+09:00","", $datetimebegin)));
                    if ($datetimebegin_tmp > $thedate) {
                        $nextflag = false;
                        continue;
                    }
                    $datetimeend = $oneVal['Route']['Line']['ArrivalState']['Datetime']['text'];
                    $timeOnBoard = $oneVal['Route']['Line']['timeOnBoard'];
                    if (!empty($oneVal['Route']['Line']['Number'])){
                        $ship_numbers = $oneVal['Route']['Line']['Number'];
                    }
                    $starttime = (isset($starttime) && $starttime > $datetimebegin) ? $starttime : $datetimebegin;
                    $datetimebegin = str_replace("T"," ", $datetimebegin);
                    $datetimebegin = str_replace("+09:00","", $datetimebegin);
                    $datetimeend = str_replace("T"," ", $datetimeend);
                    $datetimeend = str_replace("+09:00","", $datetimeend);
                    $date = Date('Y-m-d');
                    $csv = "{$one},{$onedes},{$date},{$datetimebegin},{$datetimeend},{$timeOnBoard},{$price},{$corporation},{$ship_numbers},{$typeName}";
                    $data = explode("," ,$csv);
                    $nameCompany = explode("・" ,$data[7])[0];
                    $typeService = explode("(",$data[9])[0];
                    $idType = 3;
                    if (!empty($typeService == '超高速船')){
                        $idType = 2;
                    }elseif (!empty($typeService == '旅客船')){
                        $idType = 1;
                    }
                    $departure = Wp_tokyo_stations::where('code', $data[0])->first();
                    $arrival = Wp_tokyo_stations::where('code', $data[1])->first();
                    $serviceCompany = Wp_tokyo_service_companies::where('name', $nameCompany)->first();
                    $dataInsert = [];
                    $dataInsert['departure_id'] = $departure['id'];
                    $dataInsert['arrival_id'] =  $arrival['id'];
                    $dataInsert['date'] = $data[2];
                    $dataInsert['departure_time'] = $data[3];
                    $dataInsert['arrival_time'] = $data[4];
                    $dataInsert['price'] = $data[6];
                    $dataInsert['transportation_type_id'] = $idType;
                    $dataInsert['corporation'] = $nameCompany;
                    $dataInsert['ship_number'] = $data[8];
                    $dataInsert['service_company_id'] = $serviceCompany['id'];
                    $dataInsert['price_label'] = $data[6];
                    $dataInsert['status'] = 1;
                    $dataInsert['created_at'] = $date;
                    $dataInsert['updated_at'] = $date;
                    Wp_tokyo_route_informations::create($dataInsert);
                    echo "get data chirldren success";
                }
            }
        }
    }
}
