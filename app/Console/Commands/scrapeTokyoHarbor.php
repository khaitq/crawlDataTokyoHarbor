<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CompaniesScrapingData\Tokaikisen;
use App\CompaniesScrapingData\DataFlightKeyword;
use App\CompaniesScrapingData\Shinshin_kisen;
use App\CompaniesScrapingData\Tohoair_tal;
use App\CompaniesScrapingData\Izu_syotou;
use App\CompaniesScrapingData\Central_air;
use App\CompaniesScrapingData\Ogasawarakaiun;
use App\CompaniesScrapingData\Niijima_nishiki;
use App\CompaniesScrapingData\ANA;



class scrapeTokyoHarbor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:tokyo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'crawl data from 8 website';

    /*
     *
     */
    protected $tokaikisen;
    protected $dataFlightKeyword;
    protected $shinshin_kisen;
    protected $tohoair_tal;
    protected $izu_syotou;
    protected $central_air;
    protected $ogasawarakaiun;
    protected $niijima_nishiki;
    protected $ana;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        Tokaikisen $tokaikisen,
        DataFlightKeyword $dataFlightKeyword,
        Shinshin_kisen $shinshin_kisen,
        Tohoair_tal $tohoair_tal,
        Izu_syotou $izu_syotou,
        Central_air $central_air,
        Ogasawarakaiun $ogasawarakaiun,
        Niijima_nishiki $niijima_nishiki,
        ANA $ana

    )
    {
        parent::__construct();
        $this->tokaikisen = $tokaikisen;
        $this->dataFlightKeyword = $dataFlightKeyword;
        $this->shinshin_kisen = $shinshin_kisen;
        $this->tohoair_tal = $tohoair_tal;
        $this->izu_syotou = $izu_syotou;
        $this->central_air = $central_air;
        $this->ogasawarakaiun = $ogasawarakaiun;
        $this->niijima_nishiki = $niijima_nishiki;
        $this->ana = $ana;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        /*
//         * @description call function insertDataTableFlightInforKeyword.
//         */
//        $this->dataFlightKeyword->insertDataTableFlightInforKeyword();
//
        /*
         * @description send url Shinshin_kisen for function getMessagesStatusShinshin_kisen and handel get data.
         */
        $urlShinshin_kisen = 'http://shinshin-kisen.jp/';
        $this->shinshin_kisen->getMessageStatusShinShin_kisen($urlShinshin_kisen);

        /*
         * @description send url Tokaikisen for function getMessagesStatusTokaikisen and handel get data.
         */
        $urlTokaikisen = 'https://www.tokaikisen.co.jp/schedule/';
        $this->tokaikisen->getMessagesStatusTokaikisen($urlTokaikisen);

        /*
         * @description send url Tohoair for function getMessagesStatusTohoair and handel get data.
         */
        $urlTohoair = 'http://tohoair-tal.jp/';
        $this->tohoair_tal->getMessagesStatusTohoair($urlTohoair);

        /*
         * @description send two url Izu-syotou for function getMessagesStatusIzu_syotou and handel get data.
         */
        for($i = 1; $i < 3; $i ++){
            $urlIzu_syotou = 'http://www.izu-syotou.jp/route0'.$i.'/index.html';
            $this->izu_syotou->getMessagesStatusIzu_syotou($urlIzu_syotou);
        }

        /*
         * @description call function getMessagesStatusOgasawarakaiun.
         */
         $this->ogasawarakaiun->getMessagesStatusOgasawarakaiun();

        /*
         * @desciption send url Central-air for function getMessagesStatusCentral_air and handel get data.
         */
        $urlCentral_air = 'https://www.central-air.co.jp/renew/flight_information_contents.html';
        $this->central_air->getMessagesStatusCentral_air($urlCentral_air);

        /*
         * @description call function getMessagesStatusANA.
         */
        $this->ana->getMessagesStatusANA();

        /**
         * @description call function getMessagesStatusNiijima_nishiki.
         */
        $this->niijima_nishiki->getMessagesStatusNiijima_nishiki();

    }
}

