<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CompaniesScrapingData\ApiDirectEkispert;
use App\CompaniesScrapingData\ApiEkispert;

class ScrapingApiTokyo extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraping:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run tool code api and handel data';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $apiDirectEkispert;
    protected $apiEkispert;

    public function __construct
    (
        ApiDirectEkispert $apiDirectEkispert,
        ApiEkispert $apiEkispert
    )
    {
        parent::__construct();
        $this->apiDirectEkispert = $apiDirectEkispert;
        $this->apiEkispert = $apiEkispert;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        * @description send url api 8 website for function crawlDataApi
        */
        $baseURL = "http://api.ekispert.jp/v1/json/search/course/extreme?sort=transfer&answerCount=20&conditionDetail=T3111111232119:F332112212000:A21121141:&resultDetail=addCorporation&addOperationLinePattern=true&checkEngineVersion=true&gcs=tokyo&key=test_NcQdMcXLJ3L";
//        $this->apiDirectEkispert->crawlDataApi($baseURL);
        /*
         *  @description call function crawlDataApi.
         */
        $this->apiEkispert->crawlDataApi($baseURL);
    }
}
