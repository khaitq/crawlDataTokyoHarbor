<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wp_tokyo_route_transfer_information extends Model
{
    protected $table = 'wp_tokyo_route_transfer_information';
    protected $fillable = [
        'route_id',
        'transfer_id',
        'stage'
    ];
}
