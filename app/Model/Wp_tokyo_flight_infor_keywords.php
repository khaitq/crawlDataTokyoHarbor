<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wp_tokyo_flight_infor_keywords extends Model
{
    protected $table = 'wp_tokyo_flight_infor_keywords';
    protected $fillable = ['company_id', 'keyword'];
}
