<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wp_tokyo_alert_message extends Model
{
    protected $table = 'wp_tokyo_alert_messages';
    protected $fillable = ['company_id','message','update_time','status_info',];
}
