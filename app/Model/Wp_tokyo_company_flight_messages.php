<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wp_tokyo_company_flight_messages extends Model
{
    protected $table = 'wp_tokyo_company_flight_messages';
    protected $fillable = ['company_id', 'status_info','route_information_id','message'];
}
