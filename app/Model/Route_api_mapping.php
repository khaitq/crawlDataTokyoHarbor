<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Route_api_mapping extends Model
{
    protected $table = 'route_api_mapping';
    protected $fillable = ['departure_code', 'arrival_code', 'type'];
}
