<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWpTokyoRouteTransferInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wp_tokyo_route_transfer_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('route_id')->nullable();
            $table->integer('transfer_id')->nullable();
            $table->integer('stages')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wp_tokyo_route_transfer_information');
    }
}
